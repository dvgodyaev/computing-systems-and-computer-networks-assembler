Version 4
SHEET 1 1092 680
WIRE 144 112 -16 112
WIRE 64 192 -16 192
WIRE 144 192 144 176
WIRE 144 192 64 192
WIRE 64 224 64 192
FLAG 64 224 0
SYMBOL diode 128 112 R0
SYMATTR InstName D1
SYMATTR Value dmitry
SYMBOL voltage -16 96 R0
SYMATTR InstName V1
SYMATTR Value 1
TEXT -280 24 Left 2 !.model dmitry D(IS=5e-12 N=1.11 BV=6 IBV=3e-3 RS=25e-3 TT=10e-9 CJO=5e-12)
TEXT -282 248 Left 2 !.dc V1 -6.7 1 0.01
TEXT 264 96 Left 2 !.TEMP -60 25 100
