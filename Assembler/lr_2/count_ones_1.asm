.386
 .model flat, stdcall
 option casemap :none
 include \masm32\include\windows.inc
 include \masm32\include\user32.inc
 include \masm32\include\kernel32.inc
 includelib \masm32\lib\user32.lib
 includelib \masm32\lib\kernel32.lib
 .data
    num dw 220
    msg1 db "=%d",0
    sResult byte 50 dup (?)
    Zagolovok db "result",0
    sfc db "������ %.3d",10,"����� ������ %.3X ", 0
    result db 0
 .code
 start: 
                mov     ax,num          ; �����
        CountLoop:
                test    ax, 1           ; ��������� ��������� ��� �����
                jnz     add_ones        ; ����� 1-�� - ����� �������� - ��������� � ���������� ����� ������
                jmp     next            ; ��������� � ��������� ���������� ����
        add_ones:
                inc     result

        next:
                shr     ax, 1           ; �������� AX ������ �� 1 ���
                mov     cx, ax          ; � ������� ����� ���������� AX � CX
                jcxz    output          ; ���� � AX ��� ���� - �������� ��� ��� ��� - ������� �� ������
                
                jmp     CountLoop       ; �� ���� - ��� �������� ���-�� ���������

        output:       
                invoke wsprintfA, ADDR sResult, ADDR sfc, num, result 
                invoke MessageBox, NULL, ADDR sResult, ADDR Zagolovok, MB_OK
                invoke ExitProcess,NULL
 end start ;����� ���������