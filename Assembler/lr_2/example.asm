.386
 .model flat, stdcall
 option casemap :none
 include \masm32\include\windows.inc
 include \masm32\include\user32.inc
 include \masm32\include\kernel32.inc
 includelib \masm32\lib\user32.lib
 includelib \masm32\lib\kernel32.lib
 .data
    A DD -1, -2, -3, -4, 1, 2, 0, 4, 2000, 4096, 65535, 0
    k_str dD 3
    k_stolb dD 4
    KOL_ZERO Dd 0
    Zagolovok db "programm result",0
    sResult byte 50 dup (?)
    sfc db "� ������� � ������� ��������� - %.1li ��.",0
.code
start: XOR EBX,EBX
    xor ebx, ebx
    mov ecx, 32

shifting:
    shl eax, 1
    jnc skip_inc
    inc ebx
skip_inc:
    loop shifting
    
    invoke MessageBox, NULL, ADDR sResult, addr Zagolovok, MB_OK

    invoke wsprintf, ADDR sResult, ADDR sfc, KOL_ZERO

    invoke ExitProcess, NULL
end start ;����� ���������