.data
	Array: .word 3,5,6,7,9,10
	ArraySize: .word 6
	Barray:  .word 0:6
	BAdressArray: .word 0:6
	head: .asciiz "\n There isn't such elements"
	border: .word 2
.text
#t0 ��� �������
#t1 ������� ��� ����� ������ �����
#t2 ��� ����� ������ (2)
#t3 ��� �������� �������� �������
#t4 ��� �������� ���������� ������
#t5 ��� ����� �������
#t6 ��� �������� ����� � 2-�� ���������
	la $t0, Array  			#load array's adress
	la $t5, ArraySize 		#load array size
	lw $t5, 0($t5)			#load array values
	
	addi $t7, $zero, 0		# prepare counter for new array(B array)
	
	la $t2, border			#load the number of ones
	lw $t2, 0($t2)		
	
	Arrayloop:			#starting th loop
	lw $t3, 0($t0)			#load first elem of array - we will change it
	lw $a3, 0($t0)			#load first elem of array for non-changing register
	
	Countloop:
	andi    $t1, $t3, 1		#catch the first bit of t3 register
	beq	$t1, $zero, next	#check if its 0
	addiu 	$t4, $t4, 1		#if true -> increase counter t4
	
	
	next:
	srl 	$t3,$t3, 1		#shift right t3 register
	beq 	$t3, $zero, met1	#check if t3 register is equal to zero (no valuable ones)
	j Countloop 			#if there are ones -> make a loop again
	
	catchNum:		
	addi 	$t7, $t7, 4		#increase index of arrays - t7	
	addi 	$t6, $t6, 1		#if there are 2 ones -> increase the final counter
	andi 	$t4, $t4, 0		#make t4 counter = 0
	sw 	$a3, Barray($t7)	#Load data from non-changed a3 to B array indexed in t7
	sw	$t7, BAdressArray($t7)	#Load adress to B adress array
	
	met1:
	beq  	$t4, $t2, catchNum	#check if there only 2 ones
	andi 	$t4, $t4, 0		#make t4 counter = 0
	
	addi 	$t5, $t5, -1 		#decrease size of array
 	addi 	$t0, $t0, 4 		#increase index of array
 	bgtz 	$t5, Arrayloop 		#if t5 > 0 continue loop
 	
 	beq $t6, $zero, ZeroOut
 	add $a0, $t6, $zero 
 	li $v0, 1 
 	j exit 
	ZeroOut:
	la $a0, head 
 	li $v0, 4
 	exit:
 	syscall 
 	