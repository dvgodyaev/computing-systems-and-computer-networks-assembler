.data
	num: .word 3
	border: .word 2
	head: .asciiz "\n There is such elements"
.text
	la $t1, num
	lw $t1, 0($t1)
	
	la $t2, border
	lw $t2, 0($t2)
	loop:
		andi    $t3, $t1, 1
		beq	$t3, $zero, next
		addiu 	$t4, $t4, 1
		beq	$t4, $t2, catchNum
		
	next:
		srl 	$t3,$t3, 1
		j loop 
	catchNum:	
 		la $a0, head 
 		li $v0, 4
 		syscall  