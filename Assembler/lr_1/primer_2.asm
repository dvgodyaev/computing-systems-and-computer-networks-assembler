.386
 .model flat, stdcall
 option casemap :none
 include \masm32\include\windows.inc
 include \masm32\include\user32.inc
 include \masm32\include\kernel32.inc
 includelib \masm32\lib\user32.lib
 includelib \masm32\lib\kernel32.lib
 .data
MEM1 DW 220 ;���������� � ������ �������� Word=16 ���=2 ����
 MEM2 DW 20 ;���������� � ������ �������� 2 ����
 str1 db "��������� ���������",0 ;������ � ������, �������� �������� (db)
str2 db "����� ���1 ������ ���2",0
str3 db "����� ���1 �� ������ ���2",0
 .code
start: ;����� ������ ���������
SUB DX,DX ;��������� ����������� �������� DX �� ������ ���� (���������).
;��������� ���������� �� ����� ������� ��������: DX=DX-EDX.
MOV AX,MEM1 ;��������� ����������� �� ������ �� � ������� ���1 � ������� ��.
DIV MEM2 ;������� 4 ���� (DX:AX) �� 2 ����� �� �� (���2). �� � �������, DX - �������.
 CMP DX,0 ;��������� DX � ���� � ��������� ������ (�����, ����� EFL.ZF=1)
 JZ ZERO ;�������� ������� �� ��������� (��� ZF=1) �� ����� ZERO (������),
 ;����� (�� �����, �� ������) ������� � ��������� ������� ���������.
 invoke MessageBox, NULL, addr str3, addr str1, MB_OK
JMP STOP ;����������� ������� �� ����� STOP.
ZERO: invoke MessageBox, NULL, addr str2, addr str1, MB_OK
STOP: invoke ExitProcess, NULL ;����� ���������, ����������� ������ ���������
end start ;����� ���������