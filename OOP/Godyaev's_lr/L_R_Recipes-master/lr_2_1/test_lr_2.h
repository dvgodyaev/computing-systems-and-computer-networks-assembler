#ifndef TEST_LR_2_H
#define TEST_LR_2_H
#ifndef TESTS_H
#define TESTS_H

#include <iostream>
#include <QRandomGenerator>
#include <QStringList>
#include <QTextStream>
#include <cmath>
#include "counter.h"
#define MIN_PARAM2_VALUE  -100.
#define MAX_PARAM2_VALUE +1000.
namespace Random
{

const QString& itemFrom(const QStringList &list) {
    return list.at(QRandomGenerator::global()->bounded(list.length()));
}

QString name() {
    static QStringList names = {"Aleksandr", "Aleksey", "Anatoly", "Vadim",
                                "Vasily", "Gennady", "Gleb", "Ivan",
                                "Kirill", "Konstantin", "Moisey", "Oleg"};
    return itemFrom(names);
}
};


namespace Test
{
void run()
{
    Counter<QString> defaultObject;
    assert(defaultObject.getCollectionSize() == 0);
    Counter<QString> randomObject;
    for (int i = 0; i < 100; i++)
    {
        QString ii = Random::name();
        randomObject << ii;
        Counter<QString> copyObject(randomObject);
        assert(randomObject == copyObject);
        copyObject.removeAll();
        assert(copyObject == defaultObject);
        randomObject <<("QWERTY");
        assert(!(copyObject == randomObject));
        randomObject.deleteElem("QWERTY");
        assert(!randomObject.isKeyExist("QWERTY"));
        randomObject.writeTXTFile("C:\\lr_2\\example1.txt");
        copyObject.readTXTFile("C:\\lr_2\\example1.txt");
        copyObject.writeTXTFile("C:\\lr_2\\example2.txt");
        assert(randomObject == copyObject);
    }
    std::cout << "All tests are done! ";
}

};
#endif // TESTS_H

#endif // TEST_LR_2_H
