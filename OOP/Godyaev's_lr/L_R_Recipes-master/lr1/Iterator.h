#include "Node.h"
#ifndef OwnIter_H
#define OwnIter_H
class OwnIter
{
private:
    Node *current;
public:
    OwnIter(Node*);
    void next();
    AbstractIngridient* get_curr();
};
#endif
