#include "Ingridient.h"
#include <QString>
#include "AbstractIngridient.h"
#ifndef ACTION_H
#define ACTION_H
class Action : public AbstractIngridient
{
public:
    Action();
    Action(const QString, const int);
    Action(const Action&);
    ~Action() override;
    int get_time() const;
    void set_time(int);
    bool get_obj_class() override;
    void xml_file_write(QXmlStreamWriter& xml_writer, int i) override;
    Action* xml_file_read(QXmlStreamReader&);
private:
    int action_time;
};

#endif // ACTION_H
