#include "action.h"
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QXmlStreamAttribute>
Action::Action() : AbstractIngridient()
{
    name = "take";
    action_time = 1;
}
Action::Action(const QString a, const int b) : AbstractIngridient(a)
{
    name = "take";
    action_time = 1;
    name = a;
    action_time = b;
}
Action::Action(const Action& copy) : AbstractIngridient(copy)
{
    set_time(copy.get_time());
}
Action :: ~Action(){}
int Action::get_time() const
{
    return  action_time;
}

void Action::set_time(int time)
{
    action_time = time;
}
bool Action::get_obj_class()
{
    return false;
}
void Action::xml_file_write(QXmlStreamWriter& xml_writer, int i)
{
    xml_writer.writeStartElement("Action");
        xml_writer.writeAttribute("number", QString :: number(i));
        xml_writer.writeStartElement("Name");
        xml_writer.writeCharacters(get_name());
        xml_writer.writeEndElement();
        xml_writer.writeStartElement("Time");
        xml_writer.writeCharacters(QString :: number(get_time()));
        xml_writer.writeEndElement();
    xml_writer.writeEndElement();
}
Action* Action::xml_file_read(QXmlStreamReader& xml_reader)
{
    Action* current = new Action;
    xml_reader.readNextStartElement();
    if (xml_reader.name() == "Name")
        current->set_name(xml_reader.readElementText());
    xml_reader.readNextStartElement();
    if (xml_reader.name() == "Time")
        current->set_time(xml_reader.readElementText().toInt());
    xml_reader.readNextStartElement();
    return current;
}




