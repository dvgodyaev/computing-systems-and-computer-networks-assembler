#include "Recipe.h"
#include <iostream>
#include <cassert>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QXmlStreamAttribute>
#include <QFile>
#include <QVariant>
using namespace std;
#ifndef Recipe_cpp
#define Recipe_cpp
Recipe::Recipe()
{
    head = nullptr;
    tail = nullptr;
    count = 0;
}
Recipe ::Recipe(const Recipe& recipe) :
    head(nullptr), tail(nullptr), count(0)
{
    if (recipe.head != nullptr)
    {
        Node *tmp = new Node(*recipe.head);
        for(int i=0; i< recipe.count; i++)
        {
            if (tmp->data->get_obj_class()==1)
            {
                Ingridient* curr = dynamic_cast<Ingridient*>(tmp->data);
                push(curr);
            }
            if (tmp->data->get_obj_class()==0)
            {
                Action* curr = dynamic_cast<Action*>(tmp->data);
                push(curr);
            }
            tmp = tmp->next;
        }
    }
}
Recipe ::~Recipe()
{
    Node *tmp = head;
    while (tmp != nullptr)
    {
        tmp = head->next;
        head = tmp;
    }
    count = 0;
}
void Recipe ::push(Ingridient *item)
{
    if (item != nullptr)
    {
        Node *current = new Node();
        Ingridient *tmp = new Ingridient(*item);
        current->data = tmp;
        if (head == nullptr)
        {
            head = tail = current;
            count++;
        }
        else
        {
            tail->next = current;
            tail = current;
            count++;
        }
    }
}
void Recipe ::push(Action *item)
{
    if (item != nullptr)
    {
        Node *current = new Node();
        Action *tmp = new Action(*item);
        current->data = tmp;
        if (head == nullptr)
        {
            head = tail = current;
            count++;
        }
        else
        {
            tail->next = current;
            tail = current;
            count++;
        }
    }
}
AbstractIngridient *Recipe::pop()
{
    if (head != nullptr)
    {
        AbstractIngridient *tmp1;
        tmp1 = head->data;
        head = head->next;
        count--;
        return tmp1;
    }
    else
    {
    tail = nullptr;
    head = nullptr;
    }

}
AbstractIngridient *Recipe::peek() const
{
    if (head != nullptr)
    {
        return head->data;
    }
    else
        exception();
}
void Recipe :: erase()
{
    if (head != nullptr)
    {
        Node* tmp;
        while(head->next != nullptr)
        {
            tmp = head;
            head = head->next;
            delete tmp->data;
            delete tmp;
        }
        head = nullptr;
        tail = nullptr;
        count = 0;
    }
}
int Recipe ::get_elem_count() const
{
    return count;
}
AbstractIngridient *Recipe::get_item(int tmp) const
{
    Node* curr = head;
    for (int i = 1; i<= tmp; i++)
    {
        assert( curr == nullptr);
        curr = curr->next;
    }
    return curr->data;
}
bool Recipe :: operator!=(nullptr_t)
{
    if (head != nullptr)
        return true;
    else
        return false;
}
OwnIter Recipe  :: begin()
{
    OwnIter iter(head);
    return iter;
}
OwnIter Recipe  :: end()
{
    OwnIter iter(tail);
    return iter;
}
OwnIter :: OwnIter(Node *tmp)
{
    current = tmp;
}
void OwnIter :: next()
{
    current = current->next;
}
AbstractIngridient *OwnIter::get_curr()
{
    if (current->data != nullptr)
        return current->data;
    else
        exception();
}

void Recipe ::xml_file_write(const QString filename) const
{
    QFile file(filename);
    map<Units,QString> units_to_string;
    units_to_string.insert(pair<Units, QString>(kg, "kg"));
    units_to_string.insert(pair<Units, QString>(l, "l"));
    units_to_string.insert(pair<Units, QString>(units, "units"));
    file.open(QIODevice::WriteOnly);
    QXmlStreamWriter xml_writer(&file);
    xml_writer.setAutoFormatting(true);
    xml_writer.writeStartDocument();
    xml_writer.writeStartElement("Recipe");
    Node *tmp = head;
    for (int i = 0; i< count; i++)
    {
        tmp->data->xml_file_write(xml_writer, i);
        tmp = tmp->next;
    }
    xml_writer.writeEndElement();
    xml_writer.writeEndDocument();
    file.close();
}
void Recipe :: xml_file_read(const QString filename)
{
    erase();
    QFile file(filename);
    map<QString,Units> units_to_string;
    units_to_string.insert(pair<QString,Units>("kg", kg));
    units_to_string.insert(pair<QString,Units>("l", l));
    units_to_string.insert(pair<QString,Units>("units", units));
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        exception();
    }
    else
    {
        QXmlStreamReader xml_reader;
        xml_reader.setDevice(&file);
        while(!xml_reader.atEnd())
        {
            xml_reader.readNextStartElement();
                if(xml_reader.name() == "Ingridient")
                {
                    Ingridient* current = new Ingridient();
                    current = current->xml_file_read(xml_reader);
                    push(current);
                }
                if(xml_reader.name() == "Action")
                {
                    Action* current = new Action();
                    current = current->xml_file_read(xml_reader);
                    push(current);
                }
        }
        file.close();
    }
}
bool Recipe::check_all() const
{
    if (head != nullptr)
    {
        if (!head->data->get_obj_class())
            return false;
        else
        {
            Node* curr = head;
            for (int i = 1; i < count; i++)
            {
                if (curr->data->get_obj_class()
                        && curr->next->data->get_obj_class())
                {
                    return false;
                }
                curr = curr->next;
            }
            return true;
        }
    }
    else return false;

}
bool Recipe::is_empty()
{
    if (!head && !tail)
        return true;
    else
        return false;
}
#endif // !Recipe_cpp

