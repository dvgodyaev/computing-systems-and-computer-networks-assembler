#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QDir>
#include <QFileDialog>
#include <QMenuBar>
#include <QAction>
#include "recipeshow.h"
const  QString main_dir = "C:\\Recipes";
QStringList get_list_of_files()
{
    /*
    Функция считывает список файлов в директории в QStringList.
    */
    QDir dir(main_dir);
    QStringList xml_list = dir.entryList(QStringList("*.xml"));
    for (int i = 0; i < xml_list.size(); i++)
    {
        Recipe current;
        current.xml_file_read(main_dir+"\\"+ xml_list[i]);
        bool fl = current.check_all();
        if(fl == false)
        {
            xml_list.removeAt(i);
            i--;
        }
        current.erase();
    }
    for (int i = 0; i < xml_list.size(); ++i)
    {
        xml_list[i].chop(4);
    }
    return xml_list;
}
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->listWidget->addItems(get_list_of_files());
    ui->pushButton_3->setVisible(false);
    ui->pushButton_4->setVisible(false);
    ui->pushButton_5->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_2_clicked() //close
{
    this->close();
}

void MainWindow::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
// открыть новое окно для редактирования
{
    ui->statusbar->showMessage(ui->listWidget->currentItem()->text()+ " is open");
    QString heading = item->text()+".xml";
    RecipeShow recipe_form(this, heading);
    recipe_form.setModal(true);
    recipe_form.exec();
}
void MainWindow::on_pushButton_5_clicked() // add
{
    QStringList tmp = get_list_of_files();
    QString file_name_new = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                         main_dir,
                                                         tr("XML (*.xml *)"));
    Recipe current;
    current.xml_file_read(file_name_new);
    if(!current.check_all())
    {
        ui->statusbar->showMessage("file contain a mistake!");
        current.erase();
    }
    else
    {
        file_name_new.remove(0,11);
        file_name_new.chop(4);
        bool fl = true;
        for (int i=0; i<tmp.size(); i++)
            if (tmp[i] == file_name_new)
                fl = false;
        if (fl)
            ui->listWidget->addItem(file_name_new);
        else
            ui->statusbar->showMessage("file is still exist!");
    }
}

void MainWindow::on_pushButton_clicked() // select
{
    if (ui->listWidget->currentItem())
    {
        QString heading = ui->listWidget->currentItem()->text() +".xml";
        RecipeShow recipe_form(this, heading);
        recipe_form.setModal(true);
        recipe_form.exec();
    }
}

void MainWindow::on_pushButton_3_clicked() // update
{
    ui->listWidget->clear();
    ui->listWidget->addItems(get_list_of_files());
    ui->statusbar->showMessage("recipes list updated");
}

void MainWindow::on_pushButton_4_clicked() // delete
{
    if (ui->listWidget->currentItem())
    {
        QListWidgetItem *it(ui->listWidget->currentItem());
        QFile deleted_item(main_dir + "\\" + it->text() + ".xml");
        deleted_item.remove();
        ui->listWidget->removeItemWidget(it);
        delete it;
    }
}

void MainWindow::on_actionExit_triggered() // close
{
    this->close();
}

void MainWindow::on_actionAdd_triggered() //add
{
    on_pushButton_5_clicked();
}

void MainWindow::on_actionSelect_triggered() //select
{
    on_pushButton_clicked();
}

void MainWindow::on_actionUpdate_triggered() //update
{
    on_pushButton_3_clicked();
}

void MainWindow::on_actionDelete_triggered() //delete
{
    on_pushButton_4_clicked();
}

void MainWindow::on_pushButton_6_clicked() //Edit
{
    if  (!ui->pushButton_3->isVisible())
    {
        ui->pushButton_6->setText("Edit(close)");
        ui->pushButton_3->setVisible(true);
        ui->pushButton_4->setVisible(true);
        ui->pushButton_5->setVisible(true);
    }
    else
    {
        ui->pushButton_6->setText("Edit");
        ui->pushButton_3->setVisible(false);
        ui->pushButton_4->setVisible(false);
        ui->pushButton_5->setVisible(false);
    }
}
