#ifndef Ingridients_H
#define Ingridients_H
#include <QString>
#include <qtextstream.h>
#include <map>
#include <cmath>
#include "AbstractIngridient.h"
using namespace std;
enum Units
{
    kg=0, l=1, units=2
};
class Ingridient : public AbstractIngridient
{
public:
	Ingridient();
    Ingridient(const QString, const double, const Units);
    Ingridient(const Ingridient&);
    ~Ingridient() override;
    Units get_units_of_measure() const;
    double get_quantity() const;
    void set_quantity(const double);
    void set_units(const Units);
    Ingridient& operator =(Ingridient&);
    Ingridient& operator = (nullptr_t);
    bool operator == (nullptr_t);
    bool operator != (nullptr_t);
    bool operator != (Ingridient);
    bool  get_obj_class() override;
    void xml_file_write(QXmlStreamWriter& xml_writer, int i) override;
    Ingridient* xml_file_read(QXmlStreamReader& filename);
private:
	Units units_of_measure;
    double quantity;
};
#endif // Ingridients_H
