#include "recipeshow.h"
#include "ui_recipeshow.h"
#include "Recipe.h"
#include <QMessageBox>
const  QString main_dir = "C:\\Recipes";
Recipe read_file(QString item)
{
    Recipe current;
    current.xml_file_read(main_dir+"\\"+ item);
    return current;
}
void write_file(Recipe current, QString item)
{
    current.xml_file_write(main_dir + "\\"+ item);
}
void remove_file(QString item)
{
    QFile tmp(main_dir + "\\" + item);
    tmp.remove();
}
QString show_the_recipe(QString item)
{
    map<Units,QString> units_to_string;
    units_to_string.insert(pair<Units, QString>(kg, "kg"));
    units_to_string.insert(pair<Units, QString>(l, "l"));
    units_to_string.insert(pair<Units, QString>(units, "units"));

    QString out;
    out.clear();
    Recipe first = read_file(item);
    OwnIter iter(first.begin());
    for (int i = 0; i < first.get_elem_count(); i++)
    {
        if(iter.get_curr()->get_obj_class())
        {
            Ingridient* current2 = dynamic_cast<Ingridient*>(iter.get_curr());
            out += current2->get_name()
                    + " " + QString:: number(current2->get_quantity())
                    + " " + units_to_string[current2->get_units_of_measure()] + "\n";
        }
        if(!iter.get_curr()->get_obj_class())
        {
            Action* current1 = dynamic_cast<Action*>(iter.get_curr());
            out += current1->get_name()
                    + " " + QString:: number(current1->get_time())+ " minutes \n";
        }
        iter.next();
    }
    return out;
}
RecipeShow::RecipeShow(QWidget *parent, QString tmp) :
    QDialog(parent),
    ui(new Ui::RecipeShow)
{
    ui->setupUi(this);
    ui->textEdit_recipe->setText(show_the_recipe(tmp));
    Recipe first = read_file(tmp);
    if(first.check_all() == false)
        ui->error_label->setText("Recipe contains a mistake");
    else
        ui->error_label->setText("Recipe doesn't have mistakes");
    ui->label_Recipe->setText(tmp);
    ui->line_Action->setVisible(false);
    ui->line_Ingridient->setVisible(false);
    ui->label->setVisible(false);
    ui->label_2->setVisible(false);
    ui->spinBox_quantity->setVisible(false);
    ui->spinBox_Action_time->setVisible(false);
    ui->comboBox_Units->setVisible(false);
    ui->pushButton_Action->setVisible(false);
    ui->pushButton_Ingridient->setVisible(false);
    ui->spinBox_quantity->setSingleStep(0.1);
}

RecipeShow::~RecipeShow()
{
    delete ui;
}

void RecipeShow::on_pushButton_clicked() //close
{
    this->close();
}

void RecipeShow::on_pushButton_2_clicked() //add elem
{

    if (!ui->label->isVisible())
    {
        ui->line_Ingridient->setVisible(true);
        ui->label_2->setVisible(true);
        ui->spinBox_quantity->setVisible(true);
        ui->comboBox_Units->setVisible(true);
        ui->pushButton_Ingridient->setVisible(true);
        ui->line_Action->setVisible(true);
        ui->spinBox_Action_time->setVisible(true);
        ui->pushButton_Action->setVisible(true);
        ui->label->setVisible(true);
    }
    else
    {
        ui->line_Ingridient->setVisible(false);
        ui->label_2->setVisible(false);
        ui->spinBox_quantity->setVisible(false);
        ui->comboBox_Units->setVisible(false);
        ui->pushButton_Ingridient->setVisible(false);
        ui->line_Action->setVisible(false);
        ui->spinBox_Action_time->setVisible(false);
        ui->pushButton_Action->setVisible(false);
        ui->label->setVisible(false);
    }
}

void RecipeShow::on_pushButton_Ingridient_clicked() //add ingridient
{
    if(ui->line_Ingridient->text().isEmpty()
            || ui->spinBox_quantity->value() == 0.0)
    {
        QMessageBox::critical(this, "Error", "Some fields are empty");
    }
    else
    {
        map<QString,Units> units_to_string;
        units_to_string.insert(pair<QString,Units>("kg", kg));
        units_to_string.insert(pair<QString,Units>("l", l));
        units_to_string.insert(pair<QString,Units>("units", units));
        Recipe current = read_file(ui->label_Recipe->text());
        OwnIter iter(current.end());
        if (!current.is_empty())
        {
            if (iter.get_curr()->get_obj_class())
                QMessageBox::critical(this, "Error", "Previous element is Ingridient");
            else
            {
                Ingridient* new_elem = new Ingridient(ui->line_Ingridient->text(),
                                                      ui->spinBox_quantity->value(),
                                                      units_to_string[ui->comboBox_Units->currentText()]);
                current.push(new_elem);
                write_file(current,ui->label_Recipe->text());
                ui->textEdit_recipe->setText(show_the_recipe(ui->label_Recipe->text()));
            }
        }
        else
        {
            Ingridient* new_elem = new Ingridient(ui->line_Ingridient->text(),
                                                  ui->spinBox_quantity->value(),
                                                  units_to_string[ui->comboBox_Units->currentText()]);
            current.push(new_elem);
            write_file(current,ui->label_Recipe->text());
            ui->textEdit_recipe->setText(show_the_recipe(ui->label_Recipe->text()));
        }
    }

}

void RecipeShow::on_pushButton_Action_clicked() //add Action
{
    if(ui->line_Action->text().isEmpty()
            || ui->spinBox_Action_time->value() ==0)
    {
        QMessageBox::critical(this, "Error", "Some fields are empty");
    }
    else
    {
        Recipe current = read_file(ui->label_Recipe->text());
        Action* new_elem = new Action(ui->line_Action->text(),
                                              ui->spinBox_Action_time->value());
        current.push(new_elem);
        write_file(current,ui->label_Recipe->text());
        ui->textEdit_recipe->setText(show_the_recipe(ui->label_Recipe->text()));

    }
}

void RecipeShow::on_pushButton_3_clicked() //delete
{
    Recipe current = read_file(ui->label_Recipe->text());
    current.pop();
    write_file(current,ui->label_Recipe->text());
    ui->textEdit_recipe->setText(show_the_recipe(ui->label_Recipe->text()));
    if (current.check_all() == false)
        ui->error_label->setText("Recipe contains a mistake");
    else
        ui->error_label->setText("Recipe doesn't have mistakes");
}
