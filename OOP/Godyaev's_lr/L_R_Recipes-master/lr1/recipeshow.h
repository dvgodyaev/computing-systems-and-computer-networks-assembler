#ifndef RECIPESHOW_H
#define RECIPESHOW_H

#include <QDialog>
#include "Recipe.h"
namespace Ui {
class RecipeShow;
}

class RecipeShow : public QDialog
{
    Q_OBJECT

public:
    explicit RecipeShow(QWidget *parent = nullptr, QString tmp = nullptr);
    ~RecipeShow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();


    void on_pushButton_Ingridient_clicked();

    void on_pushButton_Action_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::RecipeShow *ui;
};

#endif // RECIPESHOW_H
