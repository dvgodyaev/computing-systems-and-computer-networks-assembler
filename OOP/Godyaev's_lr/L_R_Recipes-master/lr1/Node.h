#include "Ingridient.h"
#include <memory>
#ifndef Node_H
#define Node_H
class Node
{
public:
    Node()
    {
        data = nullptr;
        next = nullptr;
    }
    ~Node() {}
    Node* next;
    AbstractIngridient* data;
};
#endif
