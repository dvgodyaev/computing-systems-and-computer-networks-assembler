#ifndef COUNTER_H
#define COUNTER_H
#include <QString>
#include <QCoreApplication>
#include <QCryptographicHash>
#include <QTextStream>
#include <QFile>
#include <cmath>
long long getHash(const QString s);
long long getHash(const int s);

template <typename T>
struct elem
{
    int value;
    T key;
};
template <typename K>
class Counter
{
public:
    Counter()
    {
        size = 0;
        initCollection();
    }
    Counter(const Counter& tmp)
    {
        size = tmp.size - 1;
        initCollection();
        for (auto i = 0; i < static_cast<int>(pow(2, tmp.size)); i++)
            if (tmp.array[i])
            {
                elem<K> *current = new elem<K>();
                current->key = tmp.array[i]->key;
                current->value = tmp.array[i]->value;
                array[i] = current;
            }
        numOfFullSpaces = tmp.numOfFullSpaces;
    }
    ~Counter()
    {
        for (auto i = 0; i < static_cast<int>(pow(2, size)); i++)
            delete array[i];
        delete []  array;
    }
    void removeAll()
    {
        for (auto i = 0; i < static_cast<int>(pow(2, size)); i++)
            delete array[i];
        size = 0;
        initCollection();
    }
    int getCollectionSize() const
    {
        return numOfFullSpaces;
    }
    bool operator ==(const Counter& tmp)
    {
        if (size == tmp.size)
        {
            for (auto i = 0; i < static_cast<int>(pow(2, size)); i++)
                if (tmp.array[i] && searchCurrentKey(tmp.array[i]->key))
                    return false;
            return true;
        }
        else
            return false;
    }
    void operator <<(const K item)
    {
        if (isKeyExist(item) == false)
        {
            if (numOfFullSpaces >= static_cast<int>(pow(2, size)/2))
                addMemory();
            elem<K> *current  = new elem<K>();
            current->key = item;
            current->value = 1;
            auto index = searchFreeSpace(item);
            array[index] = current;
            numOfFullSpaces++;
        }
        else
        {
            auto index = searchCurrentKey(item);
            array[index]->value += 1;
        }
    }
    bool deleteElem(const K item)
    {
        for (auto i = getHash(item) % static_cast<int>(pow(2, size));
             i < static_cast<int>(pow(2, size)); i++)
            if (array[i] && array[i]->key == item)
            {
                delete array[i];
                array[i] = nullptr;
                numOfFullSpaces--;
                return true;
            }
        return false;
    }
    int operator[] (const K item) const
    {
        for (auto i = getHash(item) % static_cast<int>(pow(2, size));
             i < static_cast<int>(pow(2, size)); i++)
            if (array[i] && array[i]->key == item)
                return array[i]->value;
        return -1;
    }
    bool isKeyExist(const K item) const
    {
        for (auto i = getHash(item) % static_cast<int>(pow(2, size));
             i < static_cast<int>(pow(2, size)); i++)
            if (array[i] && array[i]->key == item)
                return true;
        return false;
    }
    void writeTXTFile(const QString filename = "C:\\lr_2\\example1.txt") const
    {
        QFile writeFile(filename);
        if (size > 1)
        {
            writeFile.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream stream(&writeFile);
            for (auto i = 0; i < static_cast<int>(pow(2, size)); i++)
                if (array[i] != nullptr)
                {
                    stream << array[i]->key << " " <<
                              QString::number(array[i]->value) << "\n";
                }
            writeFile.close();
        }
    }
    void readTXTFile(const QString filename)
    {
        removeAll();
        initCollection();
        QFile readFile(filename);
        if (readFile.exists())
        {
            readFile.open(QIODevice::ReadOnly | QIODevice::Text);
            QStringList str;
            QTextStream stream(&readFile);
            while (!readFile.atEnd())
                str  << readFile.readLine();
            numOfFullSpaces = str.size();
            for (auto i = 0; i < str.size(); i++)
            {
                QStringList oneItem = str[i].split(" ");
                if (isKeyExist(oneItem[0]) == false)
                {
                    while(numOfFullSpaces >= static_cast<int>(pow(2, size) / 2))
                        addMemory();
                    elem<K> *current = new elem<K>();
                    current->key = oneItem[0];
                    current->value = oneItem[1].toInt();
                    auto index = searchFreeSpace(oneItem[0]);
                    array[index] = current;
                }
            }
            readFile.close();
        }
    }
    QString getMaxMistakes()
    {
        QString toReturn = "false";
        if (size > 1)
        {
            auto max = 0;
            for (auto i = 0; i < static_cast<int>(pow(2, size)); i++)
                if (array[i] && array[i]->value > max)
                {
                    max = array[i]->value;
                    toReturn = array[i]->key;
                }
            return toReturn;
        }
        return toReturn;
    }
    QString getMinMistakes()
    {
        QString toReturn = "false";
        if (size > 1)
        {
            int min;
            for (auto i = 0; i < static_cast<int>(pow(2, size)); i++)
                if (array[i])
                {
                    min = array[i]->value;
                    toReturn = array[i]->key;
                    break;
                }
            for (auto i = 0; i < static_cast<int>(pow(2, size)); i++)
                if (array[i] && array[i]->value < min)
                {
                    min = array[i]->value;
                    toReturn = array[i]->key;
                }
            return toReturn;
        }
        return toReturn;
    }
    QStringList getList()
    {
        QStringList toReturn;
        for (auto i = 0 ; i < static_cast<int>(pow(2, size)); i++)
            if (array[i])
                toReturn << array[i]->key + " " +
                            QString::number(array[i]->value);
        return  toReturn;
    }
private:
    int size;
    elem<K> **array;
    long numOfFullSpaces;
    void initCollection()
    {
        size++;
        array = new elem<K>*[static_cast<int>(pow(2, size))];
        for (auto i = 0 ; i < static_cast<int>(pow(2, size)); i++)
        {
            array[i] = nullptr;
        }
        numOfFullSpaces = 0;
    }
    int searchFreeSpace(const K item) const
    {
        for(auto j = getHash(item) % static_cast<int>(pow(2, size));
            j < static_cast<int>(pow(2, size)); j++)
        {
            if (!array[j])
                return j;
        }
        return -1;
    }
    int searchCurrentKey(const K item) const
    {
        for(auto j = getHash(item) % static_cast<int>(pow(2, size));
            j < static_cast<int>(pow(2, size)); j++)
        {
            if (array[j] && array[j]->key == item)
                    return j;
        }
        return -1;
    }
    void addMemory()
    {
        elem<K> **arrayNew;
        size++;
        arrayNew = new elem<K>*[static_cast<int>(pow(2, size))];
        for (auto i = 0; i < static_cast<int>(pow(2, size)); i++)
        {
            arrayNew[i] = nullptr;
        }
        for (auto i = 0; i < static_cast<int>(pow(2, size-1)); i++)
        {
            if (array[i])
            {
                auto index = searchFreeSpace(array[i]->key);
                arrayNew[index] = array[i];
            }
        }
        delete [] array;
        array = arrayNew;
    }
};

#endif // COUNTER_H
