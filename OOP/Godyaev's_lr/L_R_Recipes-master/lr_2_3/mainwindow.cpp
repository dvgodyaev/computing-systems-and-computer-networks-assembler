#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "counter_1.h"
#include <QMessageBox>
#include <QFileDialog>
void MainWindow::newMaxMin()
{
    if (ourCounter.getMaxMistakes() != "false"
            && ourCounter.getMinMistakes() != "false")
    {
        ui->lineEditMax->setText(
                    ourCounter.getMaxMistakes()
                    + " - "
                    + QString::number(ourCounter[ourCounter.getMaxMistakes()]));
        ui->lineEditMin->setText(
                    ourCounter.getMinMistakes()
                    + " - "
                    + QString::number(ourCounter[ourCounter.getMinMistakes()]));
        ui->listWidget->clear();
        ui->listWidget->addItems(ourCounter.getList());
        ui->pushButtonDelete->setEnabled(true);
        ui->pushButtonSearch->setEnabled(true);
    }
    else
    {
        ui->listWidget->clear();
        ui->pushButtonDelete->setEnabled(false);
        ui->pushButtonSearch->setEnabled(false);
        ui->lineEditMax->setText("");
        ui->lineEditMin->setText("");
        ui->statusbar->showMessage("Extremums does not exist");
    }
}
void MainWindow::save() const
{
    QMessageBox msgBox;
    QAbstractButton *save_new_btn =
            msgBox.addButton("Save New",QMessageBox::ApplyRole);
    msgBox.setText("The document has been modified.");
    msgBox.setInformativeText("Do you want to save your changes?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int res = msgBox.exec();
    if(msgBox.clickedButton() == save_new_btn)
    {
        QString new_filename = QFileDialog::getSaveFileName(
                    nullptr, tr("Save file as"), dir, tr("XML(*.xml)"));
        ourCounter.writeTXTFile(new_filename);
    }
    if (res == QMessageBox::Save)
    {
        ourCounter.writeTXTFile(dir + file);
    }
}
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    changeFl = false;
    ui->setupUi(this);
    QString path = ui->lineEditDir->text();
    dir = path.left(path.lastIndexOf("\\")+1);
    file = path.remove(0, dir.length());
    ourCounter.readTXTFile(dir+file);
    newMaxMin();
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_pushButtonDir_clicked()
{
    if (changeFl)
        save();
    if (!ui->lineEditDir->text().isEmpty())
    {
        dir = ui->lineEditDir->text().left(
                    ui->lineEditDir->text().lastIndexOf("//")+1);
        file = ui->lineEditDir->text().remove(0, dir.length());
        ourCounter.readTXTFile(dir+file);
        newMaxMin();
        changeFl = false;
    }
    else
        QMessageBox::critical(this, "Error", "Directory path is empty");
}


void MainWindow::on_pushButtonAdd_clicked()
{
    if (ui->lineEditAdd->text() != "")
    {
        ourCounter << ui->lineEditAdd->text();
        newMaxMin();
        changeFl = true;
    }
    else
        ui->statusbar->showMessage("Add line is empty");

}

void MainWindow::on_pushButtonDelete_clicked()
{
    if (ui->listWidget->currentItem())
    {
        QString tmp = ui->listWidget->currentItem()->text();
        tmp  = tmp.left(tmp.indexOf(" "));
        if (ourCounter.deleteElem(tmp))
        {
            ui->statusbar->showMessage("Element deleted");
            newMaxMin();
            changeFl = true;
        }
    }
}

void MainWindow::on_pushButtonExit_clicked()
{
    if (changeFl)
    {
        save();
    }
    this->close();
}

void MainWindow::on_pushButtonSearch_clicked() const
{
    if (ui->lineEditSearch->text() != "")
    {
        ui->labelSearchResult->setText(
                    QString::number(ourCounter[ui->lineEditSearch->text()]));
    }
    else
        ui->statusbar->showMessage("Search line is empty");
}
