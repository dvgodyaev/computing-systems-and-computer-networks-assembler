#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "counter_1.h"
#include <QMainWindow>
typedef Counter<QString> SCounter;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButtonDir_clicked();

    void on_pushButtonAdd_clicked();

    void on_pushButtonDelete_clicked();

    void on_pushButtonExit_clicked();

    void on_pushButtonSearch_clicked() const;

private:
    Ui::MainWindow *ui;
    bool changeFl;
    SCounter ourCounter;
    QString dir, file;
    void save() const;
    void newMaxMin();
};
#endif // MAINWINDOW_H
