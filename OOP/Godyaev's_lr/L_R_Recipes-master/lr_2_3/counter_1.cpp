#include "counter_1.h"
long long getHash(const QString s)
{
    QByteArray hash = QCryptographicHash::hash((s.toLocal8Bit()), QCryptographicHash::Md5);
    long long ii = 0;
    for (int i = 0; i< hash.size(); i++)
    {
        ii += static_cast<long long>(abs(hash[i]*pow(10,i)));
    }
    return ii;
}
long long getHash(const int s)
{
    QString stroke = QString::number(s);
    QByteArray hash = QCryptographicHash::hash((stroke.toLocal8Bit()), QCryptographicHash::Md5);
    long long ii = 0;
    for (int i = 0; i< hash.size(); i++)
    {
        ii += static_cast<long long>(abs(hash[i]*pow(10,i)));
    }
    return ii;
}
