#include "rate.h"

Rate::Rate()
{
    sum = 0;
}
Rate::~Rate() {}
double Rate::get_sum() const
{
    return sum;
}
double Rate::get_default() const
{
    return default_rate;
}
void Rate::count(const double tmp)
{
    sum += default_rate * tmp/100;
}
