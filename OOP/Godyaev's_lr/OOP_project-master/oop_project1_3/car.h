#ifndef CAR_H
#define CAR_H
#include <QString>
#include <QXmlStreamWriter>
class Car
{
public:
    Car()
    {
        car_mark = "default";
        car_number = "0aaa00";
    }
    Car(const Car& tmp)
    {
        set_car_num(tmp.car_number);
        set_car_mark(tmp.car_mark);
    }
    Car(const QString tmp1, const QString tmp2)
    {
        car_mark = "default";
        car_number = "0aaa00";
        set_car_mark(tmp1);
        set_car_num(tmp2);
    }
    virtual ~Car(){}
    QString get_car_num() const
    {
        return car_number;
    }
    QString get_car_mark() const
    {
        return car_mark;
    }
    inline void set_car_num(const QString tmp)
    {
        if (tmp.size() < 9 && tmp.size() >0)
            car_number = tmp;
    }
    inline void set_car_mark(const QString tmp)
    {
        {
            car_mark = tmp;
        }
    }
    virtual int get_car_type() = 0;
    virtual void xml_write(QXmlStreamWriter&) const=0;

protected:
    QString car_number, car_mark;
};

#endif // CAR_H
