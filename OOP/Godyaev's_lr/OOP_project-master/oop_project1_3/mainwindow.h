#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "parking.h"
#include <QMainWindow>
#include <QListWidgetItem>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_change_dir_clicked();

    void on_pushButton_add_person_clicked();

    void on_pushButton_update_clicked();

    void on_pushButton_filtres_clicked();

    void on_listWidget_CarOwners_itemDoubleClicked(QListWidgetItem *item);

    void on_pushButton_delete_person_clicked();

    void on_pushButton_add_file_clicked();

    void on_pushButton_exit_clicked();

    void on_pushButton_count_clicked();

    void on_listWidget_Cars_itemDoubleClicked();

    void on_actionAdd_file_triggered();

    void on_actionChange_directory_triggered();

    void on_actionUpdate_list_triggered();

    void on_actionAdd_person_triggered();

    void on_actionDelete_person_triggered();

    void on_actionExit_triggered();

private:
    Ui::MainWindow *ui;
    QString dir;
    QString file;
    Parking main_parking;
    Rate tarif;
};
#endif // MAINWINDOW_H
