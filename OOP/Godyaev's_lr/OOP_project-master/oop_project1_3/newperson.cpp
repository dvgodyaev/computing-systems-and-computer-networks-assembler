#include "newperson.h"
#include "ui_newperson.h"
#include <QMessageBox>
NewPerson::NewPerson(QWidget *parent, int fl, CarOwner *edit_person):
    QDialog(parent),
    ui(new Ui::NewPerson)
{
    if (fl == 0) //new person
    {
        ui->setupUi(this);
        ui->label_sp_tr_type->setVisible(false);
        ui->label_tonnage->setVisible(false);
        ui->comboBox->setVisible(false);
        ui->doubleSpinBox_tonnage->setVisible(false);
        ui->spinBox_time->setValue(1);
    }
    else if (fl == 1) // select or double-click
    {
        ui->setupUi(this);
        ui->lineEdit_name->setText(edit_person->get_name());
        ui->spinBox_experience->setValue(edit_person->get_experience());
        ui->lineEdit_mark->setText(edit_person->get_vehicle()->get_car_mark());
        ui->lineEdit_car_num->setText(edit_person->get_vehicle()->get_car_num());
        ui->pushButton_save->setVisible(false);
        ui->lineEdit_mark->setEnabled(false);
        ui->lineEdit_name->setEnabled(false);
        ui->lineEdit_car_num->setEnabled(false);
        ui->spinBox_time->setEnabled(false);
        ui->spinBox_experience->setEnabled(false);
        ui->doubleSpinBox_tonnage->setEnabled(false);
        ui->comboBox->setEnabled(false);
        ui->comboBox_type->setEnabled(false);
        if (edit_person->get_vehicle()->get_car_type() == 1)
        {
            ui->comboBox_type->setCurrentIndex(0);
            ui->label_sp_tr_type->setVisible(false);
            ui->label_tonnage->setVisible(false);
            ui->comboBox->setVisible(false);
            ui->doubleSpinBox_tonnage->setVisible(false);
        }
        if (edit_person->get_vehicle()->get_car_type() == 2)
        {
            ui->comboBox_type->setCurrentIndex(1);
            ui->label_sp_tr_type->setVisible(false);
            ui->label_tonnage->setVisible(true);
            ui->comboBox->setVisible(false);
            ui->doubleSpinBox_tonnage->setVisible(true);
            Truck *tmp = dynamic_cast<Truck*>(edit_person->get_vehicle());
            ui->doubleSpinBox_tonnage->setValue(tmp->get_tonnage());
        }
        if (edit_person->get_vehicle()->get_car_type() == 3)
        {
            ui->comboBox_type->setCurrentIndex(2);
            ui->label_sp_tr_type->setVisible(true);
            ui->label_tonnage->setVisible(false);
            ui->comboBox->setVisible(true);
            ui->doubleSpinBox_tonnage->setVisible(false);
            SpecialTr *tmp = dynamic_cast<SpecialTr*>(edit_person->get_vehicle());
            ui->comboBox->itemText(tmp->get_tr_type());
        }
        ui->spinBox_time->setValue(edit_person->get_time());
    }
}

NewPerson::~NewPerson()
{
    delete ui;
}
bool NewPerson::is_exist()
{
    if (new_person != nullptr)
        return true;
    else
        return false;
}
CarOwner* NewPerson::return_co() const
{
    return new_person;
}
void NewPerson::on_pushButton_save_clicked()
{
    if (!ui->lineEdit_mark->text().isEmpty() &&
            !ui->lineEdit_car_num->text().isEmpty() &&
            !ui->lineEdit_name->text().isEmpty() &&
            ui->spinBox_experience->value() >= 0 &&
            ui->spinBox_time->value() > 0)
    {
        if (ui->comboBox_type->currentText() == "Auto")
        {
            Auto* tmp = new Auto(ui->lineEdit_mark->text(),
                                 ui->lineEdit_car_num->text());
            CarOwner* owner = new CarOwner(tmp,
                                           ui->lineEdit_name->text(),
                                           ui->spinBox_experience->value(),
                                           ui->spinBox_time->value());
            new_person = owner;
        }
        else
            if (ui->comboBox_type->currentText() == "Truck" &&
                    ui->doubleSpinBox_tonnage->value() > 0)
            {
                Truck* tmp = new Truck(ui->lineEdit_mark->text(),
                                       ui->lineEdit_car_num->text(),
                                       ui->doubleSpinBox_tonnage->value());
                CarOwner* owner = new CarOwner(tmp,
                                               ui->lineEdit_name->text(),
                                               ui->spinBox_experience->value(),
                                               ui->spinBox_time->value());
                new_person = owner;
            }
            else
                if (ui->comboBox_type->currentText() == "Special transport")
                {
                    std::map<QString, transport_type> string_to_type;
                    string_to_type.insert(std::pair<QString, transport_type>("Motobyke", Motobyke));
                    string_to_type.insert(std::pair<QString, transport_type>("ATV", ATV));
                    string_to_type.insert(std::pair<QString, transport_type>("Scooter", Scooter));

                    SpecialTr* tmp = new SpecialTr(ui->lineEdit_mark->text(),
                                                   ui->lineEdit_car_num->text(),
                                                   string_to_type[ui->comboBox->currentText()]);
                    CarOwner* owner = new CarOwner(tmp,
                                                   ui->lineEdit_name->text(),
                                                   ui->spinBox_experience->value(),
                                                   ui->spinBox_time->value());
                    new_person = owner;
                }
    }
    else
        QMessageBox::critical(this, "Error", "Some fields are empty or incorrect");
    this->close();
}

void NewPerson::on_comboBox_type_activated()
{
    if (ui->comboBox_type->currentText() == "Auto")
    {
        ui->label_sp_tr_type->setVisible(false);
        ui->label_tonnage->setVisible(false);
        ui->comboBox->setVisible(false);
        ui->doubleSpinBox_tonnage->setVisible(false);
    }
    if (ui->comboBox_type->currentText() == "Truck")
    {
        ui->label_sp_tr_type->setVisible(false);
        ui->label_tonnage->setVisible(true);
        ui->comboBox->setVisible(false);
        ui->doubleSpinBox_tonnage->setVisible(true);
    }
    if (ui->comboBox_type->currentText() == "Special transport")
    {
        ui->label_sp_tr_type->setVisible(true);
        ui->label_tonnage->setVisible(false);
        ui->comboBox->setVisible(true);
        ui->doubleSpinBox_tonnage->setVisible(false);
    }
}

void NewPerson::on_pushButton_cancel_clicked()
{
    this->close();
}
