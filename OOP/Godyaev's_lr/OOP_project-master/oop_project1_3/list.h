#ifndef LIST_H
#define LIST_H
#include <iostream>
#include <QString>
template<typename T>
class Node
{
public:
    Node()
    {
        data = nullptr;
        next = nullptr;
        prev = nullptr;
    }
    Node(T* value):data(value){}
    Node(T* value, Node *item):data(value)
    {
        if (item)
            next = nullptr;
        else
        {
            next = item->next;
            item->next = this;
        }
    }
    ~Node()
    {
        delete  data;
    }
    T* data;
    Node* next;
    Node* prev;
};

template<typename T>
class OwnIter
{
public:
    OwnIter(Node<T>* item)
    {
        current = item;
    }
    ~OwnIter() {}
    void next()
    {
        if(current->next)
            current = current->next;
    }
    void prev()
    {
        if (current->prev)
            current = current->prev;
    }
    T* get_current()
    {
        if (current->data != nullptr)
            return current->data;
        else
            std::exception();
    }

private:
    Node<T>* current;
};

template<typename TT>
class List
{
public:
    List()
    {
        first = nullptr;
        last = nullptr;
        size = 0;
    }
    ~List()
    {
        Node<TT> *tmp = first;
        while (tmp != nullptr)
        {
            tmp = first->next;
            first = tmp;
            delete first;
        }
        size = 0;
    }
    inline void insert(Node<TT>* ptr, const TT* value)
    {
        if (!ptr)
            insert_front(value);
        else
        {
            new TT*(value, ptr);
            size_up();
        }
    }
    inline void insert_front( TT* value)
    {
        if (value != nullptr)
        {
            Node<TT>* ptr = new Node<TT>(value);
            if (first == nullptr)
                first = last = ptr;
            else
            {
                ptr->prev = nullptr;
                ptr->next = first;
                first->prev = ptr;
                first = ptr;
            }
            size_up();
        }
    }
    inline void insert_end( TT* value)
    {
        if (value != nullptr)
        {
            Node<TT>* ptr = new Node<TT>(value);
            ptr->next = nullptr;
            ptr->prev = nullptr;
            if (last == nullptr)
            {
                last = first = ptr;

            }
            else
            {
                ptr->next = nullptr;
                ptr->prev = last;
                last->next = ptr;
                last = ptr;
            }
            size_up();
        }
    }
    TT* take(const TT* value)
    {
        TT* to_return;
        if(first && last)
        {
            Node<TT>* tmp(first);
            for(int i = 1; i < size+1; i++)
            {
                if (tmp->data == value)
                {
                    to_return = tmp->data;
                    if (tmp->prev)
                        tmp->prev->next = tmp->next;
                    else
                        first = first->next;
                    if (tmp->next)
                        tmp->next->prev = tmp->prev;
                    else
                        last = last->prev;
                    size_down();
                    return  to_return;
                }
                tmp = tmp->next;
            }
        }
        else
        {
            first = nullptr;
            last = nullptr;
        }
    }
    TT* take_front()
    {
        if (first)
        {
            Node<TT>* ptr = first;
            first = first->next;
            size_down();
            return ptr;
        }
        else
            std::exception();
    }
    TT* take_end()
    {
        if (last)
        {
            Node<TT>* ptr = last;
            last = last->prev;
            size_down();
            return ptr;
        }
        else
            std::exception();
    }
    TT* look_front() const
    {
        if (first)
            return first->data;
        else
            std::exception();
    }
    TT* look_end() const
    {
        if (last)
            return last->data;
        else
            std::exception();
    }
    OwnIter<TT> front()
    {
        OwnIter<TT> iter(first);
        return iter;
    }
    OwnIter<TT> end()
    {
        OwnIter<TT> iter(last);
        return iter;
    }
    void remove_all()
    {
        while (size != 0)
        {
            Node<TT>* ptr = first;
            first = first->next;
            size_down();
            delete ptr;
        }
        first = last = nullptr;
    }
    int get_size() const
    {
        return size;
    }
    void append(List<TT>& new_list)
    {
        last->next = new_list.first;
        last->next->prev = last;
        last = new_list.last;
        size +=new_list.size;
    }
private:
    Node<TT>* first;
    Node<TT>* last;
    int size;
    inline void size_up()
    {
        ++size;
    }
    inline void size_down()
    {
        --size;
    }

};

#endif // LIST_H
