#include "auto.h"

Auto::Auto() : Car(){}
Auto::~Auto(){}
Auto::Auto(const Auto& tmp): Car(tmp){}
Auto::Auto(const QString tmp1, const QString tmp2):Car(tmp1,tmp2){};
int Auto::get_car_type()
{
    return 1;
}
void Auto::xml_write(QXmlStreamWriter &stream) const
{
    stream.writeStartElement("Auto");
        stream.writeStartElement("CarNumber");
        stream.writeCharacters(car_number);
        stream.writeEndElement();
        stream.writeStartElement("CarMark");
        stream.writeCharacters(car_mark);
        stream.writeEndElement();
    stream.writeEndElement();
}
Auto *Auto::xml_read(QXmlStreamReader& stream)
{
    Auto* curr = new Auto;
    stream.readNextStartElement();
    if (stream.name() == "CarNumber")
        curr->set_car_num(stream.readElementText());
    stream.readNextStartElement();
    if (stream.name() == "CarMark")
        curr->set_car_mark(stream.readElementText());
    stream.readNextStartElement();
    return curr;
}
