#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newperson.h"
#include <QMessageBox>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    dir = ui->lineEdit_dir->text().left(
                ui->lineEdit_dir->text().lastIndexOf("\\")+1);
    file = ui->lineEdit_dir->text().remove(0, dir.length());
    main_parking.xml_read_co(dir+file);
    ui->listWidget_CarOwners->addItems(main_parking.get_co_list());
    ui->listWidget_Cars->addItems(main_parking.get_car_list());
    on_pushButton_count_clicked();
}
MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_change_dir_clicked()
{
    if (!ui->lineEdit_dir->text().isEmpty())
    {
        dir = ui->lineEdit_dir->text().left(
                    ui->lineEdit_dir->text().lastIndexOf("//"));
        file = ui->lineEdit_dir->text().remove(0, dir.length());
    }
    else
        QMessageBox::critical(this, "Error", "Directory path is empty");
}

void MainWindow::on_pushButton_add_person_clicked()
{
    NewPerson add_person_form(this, 0);
    add_person_form.setModal(true);
    add_person_form.exec();
    if (add_person_form.is_exist())
    {
        main_parking.add_elem(add_person_form.return_co());
        on_pushButton_update_clicked();
    }
}

void MainWindow::on_pushButton_update_clicked()
{
    ui->listWidget_CarOwners->clear();
    ui->listWidget_Cars->clear();
    ui->listWidget_CarOwners->addItems(main_parking.get_co_list());
    ui->listWidget_Cars->addItems(main_parking.get_car_list());
}

void MainWindow::on_pushButton_filtres_clicked()
{
    ui->listWidget_CarOwners->clear();
    ui->listWidget_Cars->clear();
    if (ui->checkBox_auto->isChecked())
    {
        ui->listWidget_CarOwners->addItems(main_parking.get_only_smt_co(1));
        ui->listWidget_Cars->addItems(main_parking.get_only_smt_car(1));
    }
    if (ui->checkBox_trucks->isChecked())
    {
        ui->listWidget_CarOwners->addItems(main_parking.get_only_smt_co(2));
        ui->listWidget_Cars->addItems(main_parking.get_only_smt_car(2));
    }
    if (ui->checkBox_special_transport->isChecked())
    {
        ui->listWidget_CarOwners->addItems(main_parking.get_only_smt_co(3));
        ui->listWidget_Cars->addItems(main_parking.get_only_smt_car(3));
    }
}


void MainWindow::on_listWidget_CarOwners_itemDoubleClicked(QListWidgetItem *item)
{
    ui->listWidget_Cars->setCurrentRow(ui->listWidget_CarOwners->currentRow());
    QString name(item->text());
    name = name.remove(name.indexOf("hours"), 6);
    name = name.remove(name.indexOf("years")-1, 6);
    CarOwner tmp (*main_parking.find_item(name));
    NewPerson edit_person_form(this, 1, &tmp);
    edit_person_form.setModal(true);
    edit_person_form.exec();
}
void MainWindow::on_pushButton_delete_person_clicked()
{
    if (ui->listWidget_CarOwners->currentItem())
    {
        QString tmp = ui->listWidget_CarOwners->currentItem()->text();
        tmp = tmp.remove(tmp.indexOf("hours"), 6);
        tmp = tmp.remove(tmp.indexOf("years")-1, 6);
        tmp.chop(1);
        CarOwner* item_to_delete(main_parking.find_item(tmp));
        main_parking.delete_elem(item_to_delete);
        on_pushButton_update_clicked();
    }
}

void MainWindow::on_pushButton_add_file_clicked()
{
    QString new_file = QFileDialog::
            getOpenFileName(this, tr("Open File"), dir, tr("XML (*.xml *)"));
    Parking *new_parking = new Parking;
    new_parking->xml_read_co(new_file);
    main_parking.fusion(*new_parking);
    on_pushButton_update_clicked();
}

void MainWindow::on_pushButton_exit_clicked()
{
    QMessageBox msgBox;
    QAbstractButton *save_new_btn =
            msgBox.addButton("Save New",QMessageBox::ApplyRole);
    msgBox.setText("The document has been modified.");
    msgBox.setInformativeText("Do you want to save your changes?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int res = msgBox.exec();
    if(msgBox.clickedButton() == save_new_btn)
    {
        QString new_filename =QFileDialog::getSaveFileName(
                    this, tr("Save file as"), dir, tr("XML(*.xml)"));
        main_parking.xml_write_co(new_filename);
    }
    if ((res = QMessageBox::Save))
    {
        main_parking.xml_write_co(dir + file);
    }
    this->close();
}


void MainWindow::on_pushButton_count_clicked()
{
    tarif = main_parking.count_sum();
    ui->lineEdit_count->setText(QString::number(tarif.get_sum()));
}

void MainWindow::on_listWidget_Cars_itemDoubleClicked()
{
    int i = ui->listWidget_Cars->currentRow();
    ui->listWidget_CarOwners->setCurrentRow(i);
    on_listWidget_CarOwners_itemDoubleClicked(ui->listWidget_CarOwners->currentItem());
}

void MainWindow::on_actionAdd_file_triggered()
{
    on_pushButton_add_file_clicked();
}

void MainWindow::on_actionChange_directory_triggered()
{
    on_pushButton_change_dir_clicked();
}

void MainWindow::on_actionUpdate_list_triggered()
{
    on_pushButton_update_clicked();
}

void MainWindow::on_actionAdd_person_triggered()
{
    on_pushButton_add_person_clicked();
}

void MainWindow::on_actionDelete_person_triggered()
{
    on_pushButton_delete_person_clicked();
}

void MainWindow::on_actionExit_triggered()
{
    on_pushButton_exit_clicked();
}
