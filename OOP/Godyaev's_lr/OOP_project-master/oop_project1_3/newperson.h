#ifndef NEWPERSON_H
#define NEWPERSON_H
#include "carowner.h"
#include <QDialog>

namespace Ui {
class NewPerson;
}

class NewPerson : public QDialog
{
    Q_OBJECT

public:
    explicit NewPerson(QWidget *parent = nullptr);
    NewPerson(QWidget *parent = nullptr, int fl = 0,
              CarOwner *edit_person = nullptr);
    ~NewPerson();
    bool is_exist();
    CarOwner* return_co()const;

private slots:
    void on_pushButton_save_clicked();

    void on_comboBox_type_activated();

    void on_pushButton_cancel_clicked();

private:
    Ui::NewPerson *ui;
    CarOwner* new_person;
};

#endif // NEWPERSON_H
