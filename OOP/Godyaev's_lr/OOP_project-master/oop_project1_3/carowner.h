#ifndef CAROWNER_H
#define CAROWNER_H
#include "car.h"
#include "auto.h"
#include "truck.h"
#include "specialtr.h"
#include <QXmlStreamWriter>

class CarOwner
{
public:
    CarOwner();
    CarOwner(const CarOwner&);
    CarOwner(const Auto*, const QString, const int, const double);
    CarOwner(const Truck*, const QString, const int, const double);
    CarOwner(const SpecialTr*, const QString, const int, const double);
    ~CarOwner();
    Car* get_vehicle() const;
    QString get_name() const;
    int get_experience() const;
    int get_time() const;
    void set_vehicle(const Auto*);
    void set_vehicle(const Truck*);
    void set_vehicle(const SpecialTr*);
    void set_name(const QString);
    void set_experience(const int);
    void set_time(const double);
    bool operator ==(CarOwner&);
    void xml_write(QXmlStreamWriter&, int);
    CarOwner* xml_read(QXmlStreamReader&);
private:
    Car* vehicle;
    QString name;
    int experience;
    int time_hours;
};

#endif // CAROWNER_H
