#ifndef AUTO_H
#define AUTO_H
#include "car.h"

class Auto : public Car
{
public:
    Auto();
    Auto(const Auto&);
    Auto(const QString, const QString);
    ~Auto() override;
    int get_car_type() override;
    void xml_write(QXmlStreamWriter&) const override;
    Auto* xml_read(QXmlStreamReader&);
};

#endif // AUTO_H
