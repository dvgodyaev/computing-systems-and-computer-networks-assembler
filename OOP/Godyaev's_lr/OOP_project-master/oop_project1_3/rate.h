#ifndef RATE_H
#define RATE_H

class Rate
{
public:
    Rate();
    ~Rate();
    double get_sum() const;
    double get_default() const;
    void count(const double);
private:
    double sum;
    double default_rate = 80.0;
};

#endif // RATE_H
