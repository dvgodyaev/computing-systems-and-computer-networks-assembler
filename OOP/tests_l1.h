#ifndef TESTS_H
#define TESTS_H

#include <float.h>
#include <iostream>
#include <QRandomGenerator>
#include <QDateTime>
#include <QStringList>

namespace Random
{

double realNumber(double from = -DBL_MAX/2, double to = DBL_MAX/2) {
    return from + (to - from) * QRandomGenerator::global()->generateDouble();
}

int integer(int from = INT_MIN, int to = INT_MAX) {
    return QRandomGenerator::global()->bounded(from, to);
}

QDateTime dateTime(QDateTime from, QDateTime to) {
    return from.addSecs(static_cast<qint64>(QRandomGenerator::global()->
                                            generate64()) % from.secsTo(to));
}

const QString& itemFrom(const QStringList &list) {
    return list.at(QRandomGenerator::global()->bounded(list.length()));
}

QString ingredient() {
    static QStringList ingredients = {"Вода", "Яйцо", "Сахар", "Молоко",
                                      "Картофель", "Соль", "Кефир", "Яблоко",
                                      "Мука", "Масло", "Сосиска", "Хлопья"};
    return itemFrom(ingredients);
}

QString material() {
    static QStringList woodTypes = {"Дуб", "Вишня", "Клён", "Орех",
                                    "Палисандр", "Сосна", "Бук",
                                    "Ясень", "Берёза", "Кедр"};
    return itemFrom(woodTypes);
}

QString aminoacidName() {
    static QStringList aminoacids = {"Глицин", "Аланин", "Валин", "Изолейцин",
                                    "Лейцин", "Пролин", "Серин", "Треонин",
                                    "Цистеин", "Метионин",
                                     "Аспарагиновая кислота", "Аспарагин",
                                    "Глутаминовая кислота", "Глутамин",
                                    "Лизин", "Аргинин", "Гистидин",
                                    "Фенилаланин", "Тирозин", "Триптофан"};
    return itemFrom(aminoacids);
}

QChar aminoacidLetter() {
    static QString letters = "GAVILPSTCMDNEQKRHFYW";
    return letters.at(QRandomGenerator::global()->bounded(letters.length()));
}

QString studentFIO() {
    static QStringList names = {"Алексей", "Анатолий", "Вячеслав", "Георгий",
                                "Евгений", "Кирилл", "Матвей", "Мухаммад",
                                "Никита", "Роман", "Сергей"},
            surnames = {"Игнатьев", "Кабисов", "Кожевников", "Лернер",
                        "Лясковский", "Сафронов", "Умбрас", "Шадько",
                        "Штырлин", "Юсупов", "Якубов"},
            patronymic = {"Александрович", "Андреевич", "Даниилович",
                          "Иванович", "Ильич", "Максимович",
                          "Павлович", "Романович", "Сергеевич", "Юрьевич", ""};
    auto randomGenerator = QRandomGenerator::global();
    return surnames.at(randomGenerator->bounded(surnames.size())) + " " +
            names.at(randomGenerator->bounded(names.size())) + " " +
            patronymic.at(randomGenerator->bounded(patronymic.size()));
}
};

inline std::string stdString(const QString &qstring) {
    return qstring.toLocal8Bit().toStdString();
}

inline char stdChar(const QChar &qchar) {
    return qchar.toLatin1();
}
/*
class YourClass {
private:
    int _param1;
    double _param2;
public:
    int param3;
    double  param4;

    YourClass();
    YourClass(int value1, double value2, int value3, double value4);
    YourClass(const YourClass &other);
    int getParameter1() const;
    double getParameter2() const;
    void setParameter1(int newValue1);
    void setParameter2(double newValue2);
};
*/
#define MIN_PARAM1_VALUE -100
#define MAX_PARAM1_VALUE +100
#define MIN_PARAM2_VALUE -1000.
#define MAX_PARAM2_VALUE +1000.

namespace Test
{
void assertValidity(const YourClass &object)
{
    assert(object.getParameter1() >= MIN_PARAM1_VALUE);
    assert(object.getParameter1() < MAX_PARAM1_VALUE);

    assert(object.getParameter2() >= MIN_PARAM2_VALUE);
    assert(object.getParameter2() < MAX_PARAM2_VALUE);
}

void assertEquality(const YourClass &first, const YourClass &second)
{
    assert(first.getParameter1() == second.getParameter1());
    assert(first.getParameter2() == second.getParameter2());

    assert(first.param3 == second.param3);
    assert(first.param4 == second.param4);
}

void assertStability(YourClass &object)
{
    int value1 = Random::integer(MIN_PARAM1_VALUE, MAX_PARAM1_VALUE);
    object.setParameter1(value1);
    assert(object.getParameter1() == value1);
    int error1 = Random::integer(MAX_PARAM1_VALUE);
    object.setParameter1(error1);
    assert(object.getParameter1() != error1);
    assert(object.getParameter1() == value1);

    double value2 = Random::realNumber(MIN_PARAM2_VALUE, MAX_PARAM2_VALUE);
    object.setParameter2(value2);
    assert(object.getParameter1() == value2);
    double error2 = Random::realNumber(MAX_PARAM2_VALUE);
    object.setParameter2(error2);
    assert(object.getParameter2() != error2);
    assert(object.getParameter2() == value2);
}

void run() {
    YourClass defaultObject;
    assertValidity(defaultObject);
    for (int i = 0; i < 1000; i++)
    {
        YourClass randomObject(Random::integer(MIN_PARAM1_VALUE - 10,
                                               MAX_PARAM1_VALUE + 10),
                               Random::realNumber(MIN_PARAM2_VALUE - 10,
                                                  MAX_PARAM2_VALUE + 10),
                               Random::integer(-1e9, 1e9),
                               Random::realNumber(-1e9, 1e9));
        assertValidity(randomObject);
        YourClass copyObject(randomObject);
        assertValidity(copyObject);
        assertEquality(randomObject, copyObject);
        assertStability(copyObject);
        assertValidity(copyObject);
    }
    std::cout << "\nВсе тесты успешно пройдены!\n";
}
};

#endif // TESTS_H
